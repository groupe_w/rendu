
<?php
header('Location: page_home.html'); 
session_start();
if (session_destroy()) {
    echo 'Session détruite !';
} else {
    echo 'Erreur : impossible de détruire la session !';
}
exit();
?>

